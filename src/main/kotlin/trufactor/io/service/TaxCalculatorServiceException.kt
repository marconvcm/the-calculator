package trufactor.io.service

class TaxCalculatorServiceException(toast: String): IllegalArgumentException(toast)