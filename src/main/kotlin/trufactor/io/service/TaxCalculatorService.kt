package trufactor.io.service

import java.math.BigDecimal
import java.math.RoundingMode

class TaxCalculatorService(private vararg val providers: TaxProvider) {

    fun calculate(incoming: BigDecimal): BigDecimal {

        if(incoming <= BigDecimal.ZERO) throw TaxCalculatorServiceException("incoming is negative or zero")

        val tax = providers.firstOrNull { it.match(incoming) }?.getTax(incoming) ?: throw TaxCalculatorServiceException("no tax provider for current incoming")
        return (incoming * tax).setScale(2, RoundingMode.HALF_EVEN);
    }
}
