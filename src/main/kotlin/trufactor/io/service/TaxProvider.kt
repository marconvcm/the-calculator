package trufactor.io.service

import java.math.BigDecimal

interface TaxProvider {

    fun match(incoming: BigDecimal): Boolean

    fun getTax(incoming: BigDecimal): BigDecimal
}