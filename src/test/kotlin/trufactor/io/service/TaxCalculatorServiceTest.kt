package trufactor.io.service

import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito
import java.math.BigDecimal

// menor ou igual 10k -> taxa 0
// maior que 10k e menor ou igual a 20k -> taxa 10
// maior que 20k -> taxa 20

class TaxCalculatorServiceTest {

    @Test
    fun `it should get tax equal 10% from incoming` () {

        val input = "15000.00".toBigDecimal()
        val expected = "1500.00".toBigDecimal()

        val provider = Mockito.mock(TaxProvider::class.java)

        Mockito.`when`(provider.getTax(input)).thenReturn(BigDecimal("0.10"))
        Mockito.`when`(provider.match(input)).thenReturn(true)

        val service = TaxCalculatorService(provider)

        val result = service.calculate(input)

        assertEquals(expected, result)
    }

    @Test
    fun `it should get tax equal 20% from incoming with 2 providers` () {

        val input = "20000.00".toBigDecimal()
        val expected = "4000.00".toBigDecimal()

        val provider1 = Mockito.mock(TaxProvider::class.java)

        Mockito.`when`(provider1.getTax(input)).thenReturn(BigDecimal("0.10"))
        Mockito.`when`(provider1.match(input)).thenReturn(false)

        val provider2 = Mockito.mock(TaxProvider::class.java)

        Mockito.`when`(provider2.getTax(input)).thenReturn(BigDecimal("0.20"))
        Mockito.`when`(provider2.match(input)).thenReturn(true)

        val service = TaxCalculatorService(provider1, provider2)

        val result = service.calculate(input)

        assertEquals(expected, result)
    }

    @Test(expected = TaxCalculatorServiceException::class)
    fun `it should not accept zero or less as incoming` () {
        val provider = Mockito.mock(TaxProvider::class.java)
        TaxCalculatorService(provider).calculate("0.0".toBigDecimal())
    }
}